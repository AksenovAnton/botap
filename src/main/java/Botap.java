import commands.BaseCommand;
import commands.UnknownCommand;
import commands.bingo.*;
import commands.dailymeme.DailyMemeCommand;
import commands.dailymeme.StopDailyMemeCommand;
import commands.music.*;
import commands.other.DeleteCommand;
import commands.other.HelpCommand;
import commands.other.PolitsturmCommand;
import commands.polls.AddReactCommand;
import commands.polls.PollCommand;
import commands.polls.StopPollCommand;
import commands.roles.*;
import controllers.DailyMemeController;
import controllers.MusicController;
import controllers.NewMemberController;
import controllers.PollController;
import controllers.ReactionRoleController;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.StatusChangeEvent;
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.MessageDeleteEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class Botap extends ListenerAdapter {

    private static JDA jda;

    private static ReactionRoleController reactionRoleController;
    private static NewMemberController newMemberController;
    private static PollController pollController;
    private static MusicController musicController;
    private static DailyMemeController dailyMemeController;

    public static void main(String[] args) {
        try {
            jda = JDABuilder.create(readToken(),
					//GatewayIntent.DIRECT_MESSAGES,
					GatewayIntent.GUILD_PRESENCES,
					GatewayIntent.GUILD_MESSAGES,
					GatewayIntent.GUILD_MESSAGE_REACTIONS,
					GatewayIntent.GUILD_EMOJIS,
					GatewayIntent.GUILD_MEMBERS,
					GatewayIntent.GUILD_VOICE_STATES
			).build();
            jda.addEventListener(new Botap());
            jda.setAutoReconnect(true);
        } catch (LoginException e) {
            System.out.println("--- FAILED TO LOGIN");
            e.printStackTrace();
        }
    }

    private static String readToken() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("token.txt"));
            String token = reader.readLine();
            reader.close();
            return token;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        reactionRoleController = new ReactionRoleController(jda);
        newMemberController = new NewMemberController(jda);
        pollController = new PollController(jda);
        musicController = new MusicController(jda);
        dailyMemeController = new DailyMemeController(jda);
    }

    @Override
    public void onStatusChange(@Nonnull StatusChangeEvent event) {
        System.out.println("--- STATUS UPDATE : " + event.getNewStatus().name() + ", old status : " + event.getOldStatus().name());
    }

    @Override
    public void onGuildLeave(@Nonnull GuildLeaveEvent event) {
        if (reactionRoleController != null) {
            reactionRoleController.onGuildLeave(event);
        }
        if (newMemberController != null) {
            newMemberController.onGuildLeave(event);
        }
        if (pollController != null) {
            pollController.onGuildLeave(event);
        }
        if (musicController != null) {
            musicController.onGuildLeave(event);
        }
    }

    @Override
    public void onTextChannelDelete(@Nonnull TextChannelDeleteEvent event) {
        if (reactionRoleController != null) {
            reactionRoleController.onChannelDelete(event);
        }
        if (pollController != null) {
            pollController.onChannelDelete(event);
        }
    }

    @Override
    public void onMessageDelete(@Nonnull MessageDeleteEvent event) {
        if (reactionRoleController != null) {
            reactionRoleController.onMessageDelete(event);
        }
        if (pollController != null) {
            pollController.onMessageDelete(event);
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().equals(jda.getSelfUser())) {
            return;
        }

        switch (event.getChannelType()) {
            case PRIVATE: {
                event.getChannel().sendMessage(
                        event.getMessage().getContentRaw().replace("<@!", "<@")
                                .replace(jda.getSelfUser().getAsMention(), event.getAuthor().getAsMention())
                ).queue();
            }
            break;
            case TEXT: {
                String rawMessage = event.getMessage().getContentRaw().replace("<@!", "<@");
                // We only react to mentions or to %
				if (rawMessage.startsWith("%")) {
					rawMessage = rawMessage.replaceFirst("%", jda.getSelfUser().getAsMention() + " ");
				}
                if (rawMessage.startsWith(jda.getSelfUser().getAsMention())) {
                    BaseCommand command = parseCommand(rawMessage, event);
                    if (command != null) {
                        command.execute();
                    } else {
                        event.getChannel().sendMessage("Did anyone call me?").queue();
                    }
                } else if (rawMessage.contains(jda.getSelfUser().getAsMention())) {
                    event.getChannel().sendMessage("Did anyone call me?").queue();
                }
            }
            break;
            default: {
                // do nothing
            }
        }
    }

    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        if (event.getUser() != null && event.getUser().equals(jda.getSelfUser())) {
            return;
        }

        if (reactionRoleController != null) {
            reactionRoleController.onReaction(event);
        }
        if (pollController != null) {
            pollController.onReaction(event);
        }
    }

    @Override
    public void onMessageReactionRemove(@Nonnull MessageReactionRemoveEvent event) {
        if (event.getUser() != null && event.getUser().equals(jda.getSelfUser())) {
            return;
        }

        if (reactionRoleController != null) {
            reactionRoleController.onReactionRemove(event);
        }
    }

    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        if (event.getUser().isBot()) {
            return;
        }

        if (newMemberController != null) {
            newMemberController.onNewMember(event);
        }
    }

    private static BaseCommand parseCommand(String rawMessage, MessageReceivedEvent event) {
        String[] commandData = rawMessage.split(" ");

        if (commandData.length > 1) {
            String command = commandData[BaseCommand.INDEX_COMMAND].toLowerCase();

            // Role commands
            if (command.equals(AddToggleableRoleCommand.COMMAND.toLowerCase())) {
                return new AddToggleableRoleCommand(event, commandData, reactionRoleController);
            } else if (command.equals(RemoveToggleableRoleCommand.COMMAND.toLowerCase())) {
                return new RemoveToggleableRoleCommand(event, commandData, reactionRoleController);
            } else if (command.equals(SetDefaultRoleCommand.COMMAND.toLowerCase())) {
                return new SetDefaultRoleCommand(event, commandData, reactionRoleController);
            } else if (command.equals(RemoveDefaultRoleCommand.COMMAND.toLowerCase())) {
                return new RemoveDefaultRoleCommand(event, commandData, reactionRoleController);
            } else if (command.equals(SetToggleTypeCommand.COMMAND.toLowerCase())) {
                return new SetToggleTypeCommand(event, commandData, reactionRoleController);
            } else if (command.equals(SetNewMemberRoleCommand.COMMAND.toLowerCase())) {
                return new SetNewMemberRoleCommand(event, commandData, newMemberController);
            }

            // React commands
            else if (command.equals(AddReactCommand.COMMAND.toLowerCase())) {
                return new AddReactCommand(event, commandData);
            } else if (command.equals(PollCommand.COMMAND.toLowerCase())) {
                return new PollCommand(event, commandData, pollController);
            } else if (command.equals(StopPollCommand.COMMAND.toLowerCase())) {
                return new StopPollCommand(event, commandData, pollController);
            }

            // Music commands
            else if (command.equals(PlayCommand.COMMAND.toLowerCase())) {
                return new PlayCommand(event, commandData, musicController);
            } else if (command.equals(PauseCommand.COMMAND.toLowerCase())) {
                return new PauseCommand(event, commandData, musicController);
            } else if (command.equals(ResumeCommand.COMMAND.toLowerCase())) {
                return new ResumeCommand(event, commandData, musicController);
            } else if (command.equals(QueueCommand.COMMAND.toLowerCase())) {
                return new QueueCommand(event, commandData, musicController);
            } else if (command.equals(SkipCommand.COMMAND.toLowerCase())) {
                return new SkipCommand(event, commandData, musicController);
            } else if (command.equals(MoveCommand.COMMAND.toLowerCase())) {
                return new MoveCommand(event, commandData, musicController);
            } else if (command.equals(RemoveCommand.COMMAND.toLowerCase())) {
                return new RemoveCommand(event, commandData, musicController);
            } else if (command.equals(ClearCommand.COMMAND.toLowerCase())) {
                return new ClearCommand(event, commandData, musicController);
            } else if (command.equals(VolumeCommand.COMMAND.toLowerCase())) {
                return new VolumeCommand(event, commandData, musicController);
            } else if (command.equals(SeekCommand.COMMAND.toLowerCase())) {
				return new SeekCommand(event, commandData, musicController);
			} else if (command.equals(DisconnectCommand.COMMAND.toLowerCase())) {
                return new DisconnectCommand(event, commandData, musicController);
            }

            // Daily meme commands
			else if (command.equals(DailyMemeCommand.COMMAND.toLowerCase())) {
				return new DailyMemeCommand(event, commandData, dailyMemeController);
			}
			else if (command.equals(StopDailyMemeCommand.COMMAND.toLowerCase())) {
				return new StopDailyMemeCommand(event, commandData, dailyMemeController);
			}

			// Bingo commands
            else if (command.equals(BingoAddCommand.COMMAND.toLowerCase())) {
                return new BingoAddCommand(event, commandData);
            } else if (command.equals(BingoClearCommand.COMMAND.toLowerCase())) {
                return new BingoClearCommand(event, commandData);
            } else if (command.equals(BingoFillCommand.COMMAND.toLowerCase())) {
                return new BingoFillCommand(event, commandData);
            } else if (command.equals(BingoRemoveCommand.COMMAND.toLowerCase())) {
                return new BingoRemoveCommand(event, commandData);
            } else if (command.equals(BingoRenameCommand.COMMAND.toLowerCase())) {
                return new BingoRenameCommand(event, commandData);
            } else if (command.equals(BingoShowCommand.COMMAND.toLowerCase())) {
                return new BingoShowCommand(event, commandData);
            }

			// Other commands
            else if (command.equals(DeleteCommand.COMMAND.toLowerCase())) {
                return new DeleteCommand(event, commandData);
            } else if (command.equals(HelpCommand.COMMAND.toLowerCase())) {
                return new HelpCommand(event, commandData);
            } else if (command.equals(PolitsturmCommand.COMMAND.toLowerCase())) {
				return new PolitsturmCommand(event, commandData);
			}

            return new UnknownCommand(event, commandData);
        }

        return null;
    }
}