package utils;

public class Utils {

    public static String toChannelId(String data) {
        return toSnowflake(data);
    }

    public static String toRoleId(String data) {
        return toSnowflake(data);
    }

    private static String toSnowflake(String data) {
        return data.replaceAll("[^0-9]", "");
    }

}
