package commands.dailymeme;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.io.File;

import commands.BaseCommand;
import controllers.DailyMemeController;

import static utils.Utils.toChannelId;

public class DailyMemeCommand extends BaseCommand {

	public static final String COMMAND = "DailyMeme";
	private static final String DESCRIPTION = "Posts an random image daily from a specified folder to a specified channel";
	private static final String USAGE_HELP = COMMAND + " <channel> <folder name> <hour to post at> [message]";

	private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 3;

	private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
	private static final int INDEX_FOLDER_NAME = BaseCommand.MIN_COMMAND_LENGTH + 1;
	private static final int INDEX_HOUR = BaseCommand.MIN_COMMAND_LENGTH + 2;
	private static final int INDEX_MESSAGE = BaseCommand.MIN_COMMAND_LENGTH + 3;

	private DailyMemeController dailyMemeController;

	private String message;

	public DailyMemeCommand(MessageReceivedEvent event, String[] commandData, DailyMemeController dailyMemeController) {
		super(event, commandData);

		this.dailyMemeController = dailyMemeController;

		validate();
	}

	private void validate() {
		if (insufficientRights()) {
			return;
		}

		if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
			return;
		}

		this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);

		StringBuilder messageBuilder = new StringBuilder();
		if (commandData.length > INDEX_MESSAGE) {
			for (int i = INDEX_MESSAGE; i < commandData.length; i++) {
				messageBuilder.append(commandData[i]);
				messageBuilder.append(" ");
			}
		}
		message = messageBuilder.toString();

		if (unknownChannel(commandData[INDEX_CHANNEL])) {
			return;
		}

		File imagesFolder = new File(DailyMemeController.IMAGES_FOLDER, commandData[INDEX_FOLDER_NAME]);
		if (!imagesFolder.exists()) {
			invalidReason = "Specified folder doesn't exist";
			isValid = false;
			return;
		}

		try {
			int hour = Integer.parseInt(commandData[INDEX_HOUR]);
			if (hour < 0 || hour > 23) {
				throw new NumberFormatException("Hour must be a number between 0 and 23");
			}
		} catch (NumberFormatException e) {
			invalidReason = "Hour must be a number between 0 and 23";
			isValid = false;
			return;
		}

		isValid = true;
	}

	@Override
	public void execute() {
		if (isValid) {
			dailyMemeController.startMemes(commandData[INDEX_CHANNEL], commandData[INDEX_FOLDER_NAME], Integer.parseInt(commandData[INDEX_HOUR]), message);
			showSuccess();
		} else {
			showError(invalidReason);
		}
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getUsageHelp() {
		return USAGE_HELP;
	}

	@Override
	public boolean requiresPermission() {
		return true;
	}

}
