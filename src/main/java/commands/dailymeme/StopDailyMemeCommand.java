package commands.dailymeme;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.io.File;

import commands.BaseCommand;
import controllers.DailyMemeController;

import static utils.Utils.toChannelId;

public class StopDailyMemeCommand extends BaseCommand {

	public static final String COMMAND = "StopDailyMeme";
	private static final String DESCRIPTION = "Stop posting random memes";
	private static final String USAGE_HELP = COMMAND + " <channel>";

	private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

	private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;

	private DailyMemeController dailyMemeController;

	public StopDailyMemeCommand(MessageReceivedEvent event, String[] commandData, DailyMemeController dailyMemeController) {
		super(event, commandData);

		this.dailyMemeController = dailyMemeController;

		validate();
	}

	private void validate() {
		if (insufficientRights()) {
			return;
		}

		if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
			return;
		}

		this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);

		if (unknownChannel(commandData[INDEX_CHANNEL])) {
			return;
		}

		isValid = true;
	}

	@Override
	public void execute() {
		if (isValid) {
			dailyMemeController.stopMemes(commandData[INDEX_CHANNEL]);
			showSuccess();
		} else {
			showError(invalidReason);
		}
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getUsageHelp() {
		return USAGE_HELP;
	}

	@Override
	public boolean requiresPermission() {
		return true;
	}

}
