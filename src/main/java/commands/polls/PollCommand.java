package commands.polls;

import commands.BaseCommand;
import controllers.PollController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.ContextException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import static utils.Utils.toChannelId;

public class PollCommand extends BaseCommand {

    public static final String COMMAND = "Poll";
    private static final String DESCRIPTION = "Adds reacts to a message to turn it into a poll. Users will be able to select only one option.";
    private static final String USAGE_HELP = COMMAND + " <#channel> <messageId> <:emote:> [:emote:] [:emote:] ...";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 3;

    private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
    private static final int INDEX_MESSAGE_ID = BaseCommand.MIN_COMMAND_LENGTH + 1;
    private static final int INDEX_REACT = BaseCommand.MIN_COMMAND_LENGTH + 2;

    private int reactsLeftToAdd = 0;
    private final List<String> missingEmotes = new ArrayList<>();

    private final PollController pollController;

    public PollCommand(MessageReceivedEvent event, String[] commandData, PollController pollController) {
        super(event, commandData);

        this.pollController = pollController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);
        for (int i = INDEX_REACT; i < commandData.length; i++) {
            this.commandData[i] = this.commandData[i].replace("<:", "").replace(">", "");
        }

        if (unknownChannel(commandData[INDEX_CHANNEL])) {
            return;
        }

        if (unknownMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID])) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            reactsLeftToAdd = commandData.length - INDEX_REACT;
            for (int i = INDEX_REACT; i < commandData.length; i++) {
                Objects.requireNonNull(jda.getTextChannelById(commandData[INDEX_CHANNEL])).addReactionById(commandData[INDEX_MESSAGE_ID], commandData[i])
                        .queue(new SuccessConsumer(commandData[i]), new ErrorConsumer(commandData[i]));
            }
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

    private void showMissingEmotesError() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String emoteName : missingEmotes) {
            stringBuilder.append(":").append(emoteName, 0, emoteName.indexOf(":") + 1).append(" ");
        }
        showError("Can't find the following emotes: %S. Please make sure they are available on this server.".replace("%S", stringBuilder.toString()));
    }

    private class SuccessConsumer implements Consumer<Void> {

        private final String emoteName;

        private SuccessConsumer(String emoteName) {
            this.emoteName = emoteName;
        }

        @Override
        public void accept(Void aVoid) {
            reactsLeftToAdd--;
            pollController.addToggleReaction(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID], emoteName);
            if (reactsLeftToAdd <= 0) {
                if (missingEmotes.isEmpty()) {
                    showSuccess();
                } else {
                    showMissingEmotesError();
                }
            }
        }
    }

    private class ErrorConsumer implements Consumer<Throwable> {

        private final String emoteName;

        private ErrorConsumer(String emoteName) {
            this.emoteName = emoteName;
        }

        @Override
        public void accept(Throwable throwable) {
            reactsLeftToAdd--;
            missingEmotes.add(emoteName);

            if (reactsLeftToAdd <= 0) {
                if (throwable.getCause() instanceof ContextException) {
                    showMissingEmotesError();
                } else {
                    showError("Unknown error happened");
                }
            }
        }
    }
}
