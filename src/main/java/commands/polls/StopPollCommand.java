package commands.polls;

import commands.BaseCommand;
import controllers.PollController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.Objects;

import static utils.Utils.toChannelId;

public class StopPollCommand extends BaseCommand {

    public static final String COMMAND = "StopPoll";
    private static final String DESCRIPTION = "Stops the poll and removes the reactions added by the bot.";
    private static final String USAGE_HELP = COMMAND + " <#channel> <messageId>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 2;

    private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
    private static final int INDEX_MESSAGE_ID = BaseCommand.MIN_COMMAND_LENGTH + 1;

    private final PollController pollController;

    public StopPollCommand(MessageReceivedEvent event, String[] commandData, PollController pollController) {
        super(event, commandData);

        this.pollController = pollController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);

        if (unknownChannel(commandData[INDEX_CHANNEL])) {
            return;
        }

        if (unknownMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID])) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            String[] reactsToRemove = pollController.getReactsForMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID]);
            if (reactsToRemove != null) {
                for (String react : reactsToRemove) {
                    Objects.requireNonNull(jda.getTextChannelById(commandData[INDEX_CHANNEL])).removeReactionById(commandData[INDEX_MESSAGE_ID], react).complete();
                }
                pollController.stopPoll(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID]);
                showSuccess();
            } else {
                showError("This message is not monitored");
            }
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

}
