package commands.bingo;

import commands.BaseCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;

public class BingoShowCommand extends BaseBingoCommand {

    public static final String COMMAND = "Bingo";
    private static final String DESCRIPTION = "Shows current bingo table";
    private static final String USAGE_HELP = COMMAND;

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    public BingoShowCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);
    }

    @Override
    public void execute() {
        showTable(readTableFromFile());
    }

    @Override
    protected int getMinCommandLength() {
        return MIN_COMMAND_LENGTH;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }
}
