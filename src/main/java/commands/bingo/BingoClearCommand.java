package commands.bingo;

import commands.BaseCommand;
import javafx.util.Pair;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.Collections;

public class BingoClearCommand extends BaseBingoCommand {

    public static final String COMMAND = "BingoClear";
    private static final String DESCRIPTION = "Clears bingo table";
    private static final String USAGE_HELP = COMMAND;

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    public BingoClearCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);
    }

    @Override
    public void execute() {
        ArrayList<Pair<String, Boolean>> table = readTableFromFile();
        for (int i = 0; i < table.size(); i++) {
            table.set(i, new Pair<>(table.get(i).getKey(), false));
        }
        Collections.shuffle(table);
        writeTableInFile(table);
        showTable(table);
    }

    @Override
    protected int getMinCommandLength() {
        return MIN_COMMAND_LENGTH;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }
}
