package commands.bingo;

import commands.BaseCommand;
import javafx.util.Pair;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.ArrayList;

public class BingoRenameCommand extends BaseBingoCommand {

    public static final String COMMAND = "BingoRename";
    private static final String DESCRIPTION = "Renames cell in bingo table";
    private static final String USAGE_HELP = COMMAND + " <cell number> <new name>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 2;

    private static final int INDEX_POSITION = BaseCommand.MIN_COMMAND_LENGTH;

    public BingoRenameCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);
        insufficientRights();
    }

    @Override
    public void execute() {
        if (isValid) {
            ArrayList<Pair<String, Boolean>> table = readTableFromFile();

            int targetCellNumber = Integer.parseInt(commandData[INDEX_POSITION]) - 1;
            if (targetCellNumber >= 0 && targetCellNumber < table.size()) {

                StringBuilder newText = new StringBuilder();
                for (int i = INDEX_POSITION + 1; i < commandData.length; i++) {
                    newText.append(commandData[i]).append(" ");
                }

                table.set(targetCellNumber, new Pair<>(newText.toString(), false));

                writeTableInFile(table);
                showTable(table);
                return;
            } else {
                invalidReason = "Invalid cell number";
                isValid = false;
            }
        }

        showError(invalidReason);
    }

    @Override
    protected int getMinCommandLength() {
        return MIN_COMMAND_LENGTH;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }
}