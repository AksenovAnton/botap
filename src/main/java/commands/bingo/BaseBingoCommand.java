package commands.bingo;

import commands.BaseCommand;
import javafx.util.Pair;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

abstract public class BaseBingoCommand extends BaseCommand {

    private static final String BINGO_FILE = "BingoFile";

    protected BaseBingoCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);

        validate();
    }

    private void validate() {
        if (wrongCommandLength(getMinCommandLength())) {
            return;
        }

        if (!Objects.requireNonNull(member.getVoiceState()).inVoiceChannel()) {
            invalidReason = "You are not in a voice channel";
            isValid = false;
            return;
        }

        isValid = true;
    }

    protected abstract int getMinCommandLength();

    protected ArrayList<Pair<String, Boolean>> readTableFromFile() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(BINGO_FILE)))) {
            return (ArrayList<Pair<String, Boolean>>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            return new ArrayList<>();
        }
    }

    protected void writeTableInFile(ArrayList<Pair<String, Boolean>> table) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(BINGO_FILE)))) {
            oos.writeObject(table);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void showTable(ArrayList<Pair<String, Boolean>> table) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < table.size(); i++) {
            sb
                    .append(table.get(i).getValue() ? ":white_check_mark: " : ":white_square_button: ")
                    .append(i + 1)
                    .append(") ")
                    .append(table.get(i).getKey())
                    .append(System.lineSeparator());
        }

        if (table.stream().allMatch(Pair::getValue)) {
            sb
                    .append("**BINGO !**")
                    .append(System.lineSeparator())
                    .append(System.lineSeparator())
                    .append("*type `%BingoClear` to clear table*");
        }

        MessageBuilder messageBuilder = new MessageBuilder();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(Color.GREEN);
        embedBuilder.addField(new MessageEmbed.Field(getCommand(), sb.toString(), false));
        messageBuilder.setEmbed(embedBuilder.build());
        channel.sendMessage(messageBuilder.build()).queue();
    }
}