package commands.bingo;

import commands.BaseCommand;
import javafx.util.Pair;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.ArrayList;

public class BingoFillCommand extends BaseBingoCommand {

    public static final String COMMAND = "Fill";
    private static final String DESCRIPTION = "Fills cell in bingo table";
    private static final String USAGE_HELP = COMMAND + " <cell number>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

    private static final int INDEX_POSITION = BaseCommand.MIN_COMMAND_LENGTH;

    public BingoFillCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);
    }

    @Override
    public void execute() {
        if (commandData.length > INDEX_POSITION) {
            ArrayList<Pair<String, Boolean>> table = readTableFromFile();
            int targetCellNumber = Integer.parseInt(commandData[INDEX_POSITION]) - 1;
            if (targetCellNumber >= 0 && targetCellNumber < table.size()) {
                table.set(targetCellNumber, new Pair<>(table.get(targetCellNumber).getKey(), true));
                writeTableInFile(table);
                showTable(table);
                return;
            } else {
                invalidReason = "Invalid cell number";
                isValid = false;
            }
        }
        showError(invalidReason);
    }

    @Override
    protected int getMinCommandLength() {
        return MIN_COMMAND_LENGTH;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }
}
