package commands.bingo;

import commands.BaseCommand;
import javafx.util.Pair;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.ArrayList;

public class BingoAddCommand extends BaseBingoCommand {

    public static final String COMMAND = "BingoAdd";
    private static final String DESCRIPTION = "Adds cell in bingo table";
    private static final String USAGE_HELP = COMMAND + " <name>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

    private static final int INDEX_POSITION = BaseCommand.MIN_COMMAND_LENGTH;

    public BingoAddCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);
        insufficientRights();
    }

    @Override
    public void execute() {
        if (isValid) {
            ArrayList<Pair<String, Boolean>> table = readTableFromFile();

            StringBuilder text = new StringBuilder();
            for (int i = INDEX_POSITION; i < commandData.length; i++) {
                text.append(commandData[i]).append(" ");
            }

            table.add(new Pair<>(text.toString(), false));

            writeTableInFile(table);
            showTable(table);
        }

        showError(invalidReason);
    }

    @Override
    protected int getMinCommandLength() {
        return MIN_COMMAND_LENGTH;
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }
}