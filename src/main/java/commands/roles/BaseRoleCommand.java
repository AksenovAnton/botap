package commands.roles;

import commands.BaseCommand;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.internal.utils.Checks;

abstract class BaseRoleCommand extends BaseCommand {

    BaseRoleCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);
    }

    boolean unknownRole(String role) {
        try {
            Checks.isSnowflake(role);
        } catch (IllegalArgumentException e) {
            invalidReason = "Can't find the role";
            isValid = false;
            return true;
        }

        return false;
    }

}
