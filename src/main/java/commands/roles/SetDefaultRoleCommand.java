package commands.roles;

import commands.BaseCommand;
import controllers.ReactionRoleController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import static utils.Utils.toChannelId;
import static utils.Utils.toRoleId;

public class SetDefaultRoleCommand extends BaseRoleCommand {

    public static final String COMMAND = "SetDefault";
    private static final String DESCRIPTION = "Adds a role to a user when the user removes all reactions";
    private static final String USAGE_HELP = COMMAND + " <#channel> <messageId> <@role>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 3;

    private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
    private static final int INDEX_MESSAGE_ID = BaseCommand.MIN_COMMAND_LENGTH + 1;
    private static final int INDEX_ROLE = BaseCommand.MIN_COMMAND_LENGTH + 2;

    private final ReactionRoleController reactionRoleController;

    public SetDefaultRoleCommand(MessageReceivedEvent event, String[] commandData, ReactionRoleController reactionRoleController) {
        super(event, commandData);

        this.reactionRoleController = reactionRoleController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);
        this.commandData[INDEX_ROLE] = toRoleId(this.commandData[INDEX_ROLE]);

        if (unknownChannel(commandData[INDEX_CHANNEL])) {
            return;
        }

        if (unknownMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID])) {
            return;
        }

        if (unknownRole(commandData[INDEX_ROLE])) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            reactionRoleController.setDefaultRole(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID], commandData[INDEX_ROLE]);
            showSuccess();
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

}
