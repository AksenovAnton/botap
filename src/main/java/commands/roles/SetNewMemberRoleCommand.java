package commands.roles;

import commands.BaseCommand;
import controllers.NewMemberController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import static utils.Utils.toRoleId;

public class SetNewMemberRoleCommand extends BaseRoleCommand {

    public static final String COMMAND = "NewMemberRole";
    private static final String DESCRIPTION = "Assigns roles when a new member joins the server";
    private static final String USAGE_HELP = COMMAND + " [@role, @role, ...]";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    private final NewMemberController newMemberController;

    public SetNewMemberRoleCommand(MessageReceivedEvent event, String[] commandData, NewMemberController newMemberController) {
        super(event, commandData);

        this.newMemberController = newMemberController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        for (int i = MIN_COMMAND_LENGTH; i < commandData.length; i++) {
            this.commandData[i] = toRoleId(this.commandData[i]);
        }

        for (int i = MIN_COMMAND_LENGTH; i < commandData.length; i++) {
            if (unknownRole(commandData[i])) {
                invalidReason = "One of the roles cannot be found";
                isValid = false;
                return;
            }
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            if (commandData.length > MIN_COMMAND_LENGTH) {
                String[] roles = new String[commandData.length - MIN_COMMAND_LENGTH];
                System.arraycopy(commandData, MIN_COMMAND_LENGTH, roles, 0, roles.length);
                newMemberController.setNewMemberRoles(guild.getId(), roles);
            } else {
                newMemberController.setNewMemberRoles(guild.getId(), null);
            }
            showSuccess();
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

}
