package commands.roles;

import commands.BaseCommand;
import controllers.ReactionRoleController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import static utils.Utils.toChannelId;

public class SetToggleTypeCommand extends BaseRoleCommand {

    public static final String COMMAND = "SetToggleType";
    private static final String DESCRIPTION = "Sets the reaction role toggle type. Allowed types are \"multi\" or \"single\"";
    private static final String USAGE_HELP = COMMAND + " <#channel> <messageId> <type>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 3;

    private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
    private static final int INDEX_MESSAGE_ID = BaseCommand.MIN_COMMAND_LENGTH + 1;
    private static final int INDEX_TYPE = BaseCommand.MIN_COMMAND_LENGTH + 2;

    private final ReactionRoleController reactionRoleController;

    public SetToggleTypeCommand(MessageReceivedEvent event, String[] commandData, ReactionRoleController reactionRoleController) {
        super(event, commandData);

        this.reactionRoleController = reactionRoleController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);
        this.commandData[INDEX_CHANNEL] = commandData[INDEX_CHANNEL].toLowerCase();

        if (unknownChannel(commandData[INDEX_CHANNEL])) {
            return;
        }

        if (unknownMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID])) {
            return;
        }

        if (!ReactionRoleController.TYPE_SINGLE.toLowerCase().equals(commandData[INDEX_TYPE])
                && !ReactionRoleController.TYPE_MULTI.toLowerCase().equals(commandData[INDEX_TYPE])) {
            invalidReason = "Incorrect type. Allowed types are " + ReactionRoleController.TYPE_SINGLE + " or " + ReactionRoleController.TYPE_MULTI + ".";
            isValid = false;
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            reactionRoleController.setToggleType(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID], commandData[INDEX_TYPE]);
            showSuccess();
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

}
