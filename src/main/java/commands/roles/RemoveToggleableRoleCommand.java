package commands.roles;

import commands.BaseCommand;
import controllers.ReactionRoleController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.ContextException;

import java.util.Objects;
import java.util.function.Consumer;

import static utils.Utils.toChannelId;

public class RemoveToggleableRoleCommand extends BaseRoleCommand {

    public static final String COMMAND = "RemoveToggle";
    private static final String DESCRIPTION = "Removes reaction role from a message";
    private static final String USAGE_HELP = COMMAND + " <#channel> <messageId> <:emote:>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 3;

    private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
    private static final int INDEX_MESSAGE_ID = BaseCommand.MIN_COMMAND_LENGTH + 1;
    private static final int INDEX_REACT = BaseCommand.MIN_COMMAND_LENGTH + 2;

    private final ReactionRoleController reactionRoleController;

    public RemoveToggleableRoleCommand(MessageReceivedEvent event, String[] commandData, ReactionRoleController reactionRoleController) {
        super(event, commandData);
        this.reactionRoleController = reactionRoleController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);
        this.commandData[INDEX_REACT] = this.commandData[INDEX_REACT].replace("<:", "").replace(">", "");

        if (unknownChannel(commandData[INDEX_CHANNEL])) {
            return;
        }

        if (unknownMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID])) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            reactionRoleController.removeReaction(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID], commandData[INDEX_REACT]);
            Objects.requireNonNull(jda.getTextChannelById(commandData[INDEX_CHANNEL])).removeReactionById(commandData[INDEX_MESSAGE_ID], commandData[INDEX_REACT]).queue(success, failure);
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

    private final Consumer<? super Void> success = (Consumer<Void>) aVoid -> showSuccess();

    private final Consumer<? super Throwable> failure = (Consumer<Throwable>) throwable -> {
        if (throwable.getCause() instanceof ContextException) {
            showError("Can't find the emote. Please make sure it is available on this server.");
        } else {
            showError("Unknown error happened");
        }
    };
}
