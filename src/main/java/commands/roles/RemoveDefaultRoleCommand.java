package commands.roles;

import commands.BaseCommand;
import controllers.ReactionRoleController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import static utils.Utils.toChannelId;

public class RemoveDefaultRoleCommand extends BaseRoleCommand {

    public static final String COMMAND = "RemoveDefault";
    private static final String DESCRIPTION = "Removes reaction role when a user removes all reactions";
    private static final String USAGE_HELP = COMMAND + " <#channel> <messageId>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 3;

    private static final int INDEX_CHANNEL = BaseCommand.MIN_COMMAND_LENGTH;
    private static final int INDEX_MESSAGE_ID = BaseCommand.MIN_COMMAND_LENGTH + 1;

    private final ReactionRoleController reactionRoleController;

    public RemoveDefaultRoleCommand(MessageReceivedEvent event, String[] commandData, ReactionRoleController reactionRoleController) {
        super(event, commandData);

        this.reactionRoleController = reactionRoleController;

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        this.commandData[INDEX_CHANNEL] = toChannelId(this.commandData[INDEX_CHANNEL]);

        if (unknownChannel(commandData[INDEX_CHANNEL])) {
            return;
        }

        if (unknownMessage(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID])) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            reactionRoleController.removeDefaultRole(commandData[INDEX_CHANNEL], commandData[INDEX_MESSAGE_ID]);
            showSuccess();
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

}
