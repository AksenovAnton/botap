package commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.internal.utils.Checks;

import java.awt.*;
import java.util.Objects;

public abstract class BaseCommand {

	// public static final int INDEX_BOT_MENTION = 0;
	public static final int INDEX_COMMAND = 1;

	protected static final int MIN_COMMAND_LENGTH = 2;

	protected final JDA jda;
	protected final Guild guild;
	protected final TextChannel channel;
	protected final Member member;
	protected final Message commandMessage;
	protected final String[] commandData;

	protected boolean isValid;
	protected String invalidReason;

	protected BaseCommand(MessageReceivedEvent event, String[] commandData) {
		if (event != null) {
			this.jda = event.getJDA();
			this.guild = event.getGuild();
			this.channel = event.getTextChannel();
			this.member = event.getMember();
			this.commandMessage = event.getMessage();
		} else {
			this.jda = null;
			this.guild = null;
			this.channel = null;
			this.member = null;
			this.commandMessage = null;
		}

		this.commandData = commandData;
	}

	protected boolean insufficientRights() {
		if (member == null) {
			invalidReason = "Member doesn't exist";
			isValid = false;
			return true;
		}

		if (!member.isOwner()) {
			int bestRolePosition = -1;
			for (Role role : Objects.requireNonNull(channel.getGuild().getMember(jda.getSelfUser())).getRoles()) {
				if (role.getPosition() > bestRolePosition) {
					bestRolePosition = role.getPosition();
				}
			}

			int bestCallerRolePosition = -1;
			for (Role role : member.getRoles()) {
				if (role.getPosition() > bestCallerRolePosition) {
					bestCallerRolePosition = role.getPosition();
				}
			}

			if (bestRolePosition > bestCallerRolePosition) {
				invalidReason = "You don't have permission to use this command";
				isValid = false;
				return true;
			}
		}

		return false;
	}

	protected boolean wrongCommandLength(int minLength) {
		if (commandData == null || commandData.length < minLength) {
			invalidReason = "The command is too short";
			isValid = false;
			return true;
		}

		return false;
	}

	protected boolean unknownChannel(String channelId) {
		try {
			Checks.isSnowflake(channelId, "Channel ID");
			if (jda.getTextChannelById(channelId) == null) {
				throw new IllegalArgumentException("Can't find the channel");
			}
		} catch (IllegalArgumentException e) {
			invalidReason = "Can't find the channel";
			isValid = false;
			return true;
		}

		return false;
	}

	protected boolean unknownMessage(String channelId, String messageId) {
		try {
			Checks.isSnowflake(channelId, "Channel ID");
			try {
				Objects.requireNonNull(jda.getTextChannelById(channelId)).retrieveMessageById(messageId).complete();
			} catch (ErrorResponseException e) {
				throw new IllegalArgumentException("Can't find the message");
			}
		} catch (IllegalArgumentException e) {
			invalidReason = "Can't find the message";
			isValid = false;
			return true;
		}

		return false;
	}

	public abstract void execute();

	public abstract String getCommand();

	public abstract String getDescription();

	public abstract String getUsageHelp();

	public abstract boolean requiresPermission();

	protected void showSuccess() {
		MessageBuilder messageBuilder = new MessageBuilder();
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setColor(Color.GREEN);
		embedBuilder.addField(new MessageEmbed.Field(getCommand(), "Success", false));
		messageBuilder.setEmbed(embedBuilder.build());
		channel.sendMessage(messageBuilder.build()).queue();
	}

	protected void showError(String errorMessage) {
		MessageBuilder messageBuilder = new MessageBuilder();
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setColor(Color.RED);
		embedBuilder.addField(new MessageEmbed.Field(getCommand(), errorMessage, false));
		embedBuilder.addField(new MessageEmbed.Field("Usage", getUsageHelp(), false));
		messageBuilder.setEmbed(embedBuilder.build());
		channel.sendMessage(messageBuilder.build()).queue();
	}
}
