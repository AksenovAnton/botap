package commands;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class UnknownCommand extends BaseCommand {

    private static final String COMMAND = "UnknownCommand";

    public UnknownCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);

        validate();
    }

    private void validate() {
        invalidReason = "Sorry, I don't understand the command.";
        isValid = false;
    }

    @Override
    public void execute() {
        showError(invalidReason);
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getUsageHelp() {
        return "Let the \"Help\" guide you";
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }
}
