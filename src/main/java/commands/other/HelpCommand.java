package commands.other;

import commands.BaseCommand;
import commands.music.*;
import commands.polls.AddReactCommand;
import commands.polls.PollCommand;
import commands.polls.StopPollCommand;
import commands.roles.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HelpCommand extends BaseCommand {

    public static final String COMMAND = "Help";
    private static final String DESCRIPTION = "Do you really need help on help?";
    private static final String USAGE_HELP = COMMAND + " [command]";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    private static List<CommandCategory> LIST_COMMANDS = new LinkedList<>();

    static {
        CommandCategory generalCategory = new CommandCategory("General commands");
        generalCategory.map_commands.put(HelpCommand.COMMAND.toLowerCase(), new HelpCommand(null, null));
        generalCategory.map_commands.put(DeleteCommand.COMMAND.toLowerCase(), new DeleteCommand(null, null));
		generalCategory.map_commands.put(PolitsturmCommand.COMMAND.toLowerCase(), new PolitsturmCommand(null, null));
        LIST_COMMANDS.add(generalCategory);

        CommandCategory musicCategory = new CommandCategory("Music commands");
        musicCategory.map_commands.put(PlayCommand.COMMAND.toLowerCase(), new PlayCommand(null, null, null));
        musicCategory.map_commands.put(PauseCommand.COMMAND.toLowerCase(), new PauseCommand(null, null, null));
        musicCategory.map_commands.put(ResumeCommand.COMMAND.toLowerCase(), new ResumeCommand(null, null, null));
        musicCategory.map_commands.put(QueueCommand.COMMAND.toLowerCase(), new QueueCommand(null, null, null));
        musicCategory.map_commands.put(SkipCommand.COMMAND.toLowerCase(), new SkipCommand(null, null, null));
        musicCategory.map_commands.put(MoveCommand.COMMAND.toLowerCase(), new MoveCommand(null, null, null));
        musicCategory.map_commands.put(RemoveCommand.COMMAND.toLowerCase(), new RemoveCommand(null, null, null));
        musicCategory.map_commands.put(ClearCommand.COMMAND.toLowerCase(), new ClearCommand(null, null, null));
        musicCategory.map_commands.put(VolumeCommand.COMMAND.toLowerCase(), new VolumeCommand(null, null, null));
		musicCategory.map_commands.put(SeekCommand.COMMAND.toLowerCase(), new SeekCommand(null, null, null));
        musicCategory.map_commands.put(DisconnectCommand.COMMAND.toLowerCase(), new DisconnectCommand(null, null, null));
        LIST_COMMANDS.add(musicCategory);

        CommandCategory pollsCategory = new CommandCategory("Polls commands");
        pollsCategory.map_commands.put(AddReactCommand.COMMAND.toLowerCase(), new AddReactCommand(null, null));
        pollsCategory.map_commands.put(PollCommand.COMMAND.toLowerCase(), new PollCommand(null, null, null));
        pollsCategory.map_commands.put(StopPollCommand.COMMAND.toLowerCase(), new StopPollCommand(null, null, null));
        LIST_COMMANDS.add(pollsCategory);

        CommandCategory rolesCategory = new CommandCategory("Roles commands");
        rolesCategory.map_commands.put(SetNewMemberRoleCommand.COMMAND.toLowerCase(), new SetNewMemberRoleCommand(null, null, null));
        rolesCategory.map_commands.put(AddToggleableRoleCommand.COMMAND.toLowerCase(), new AddToggleableRoleCommand(null, null, null));
        rolesCategory.map_commands.put(RemoveToggleableRoleCommand.COMMAND.toLowerCase(), new RemoveToggleableRoleCommand(null, null, null));
        rolesCategory.map_commands.put(SetDefaultRoleCommand.COMMAND.toLowerCase(), new SetDefaultRoleCommand(null, null, null));
        rolesCategory.map_commands.put(RemoveDefaultRoleCommand.COMMAND.toLowerCase(), new RemoveDefaultRoleCommand(null, null, null));
        rolesCategory.map_commands.put(SetToggleTypeCommand.COMMAND.toLowerCase(), new SetToggleTypeCommand(null, null, null));
        LIST_COMMANDS.add(rolesCategory);
    }

    public HelpCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);

        validate();
    }

    private void validate() {
        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        if (commandData.length >= MIN_COMMAND_LENGTH + 1) {
            commandData[MIN_COMMAND_LENGTH] = commandData[MIN_COMMAND_LENGTH].toLowerCase();

            boolean knownCommand = false;

            for (CommandCategory commandCategory : LIST_COMMANDS) {
                for (String command : commandCategory.map_commands.keySet()) {
                    if (commandData[MIN_COMMAND_LENGTH].equals(command)) {
                        knownCommand = true;
                        break;
                    }
                }
            }

            if (!knownCommand) {
                invalidReason = "Unknown command";
                isValid = false;
                return;
            }
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            boolean hasPermission = !insufficientRights();

            if (commandData.length >= MIN_COMMAND_LENGTH + 1) {
                MessageBuilder messageBuilder = new MessageBuilder();
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setTitle("Help");

                BaseCommand command = null;
                for (CommandCategory commandCategory : LIST_COMMANDS) {
                    command = commandCategory.map_commands.get(commandData[MIN_COMMAND_LENGTH]);
                    if (command != null) {
                        break;
                    }
                }
                assert command != null;
                if (!command.requiresPermission() || hasPermission) {
                    embedBuilder.setColor(Color.CYAN);
                    embedBuilder.addField(new MessageEmbed.Field(command.getUsageHelp(), command.getDescription(), false));
                } else {
                    embedBuilder.setColor(Color.RED);
                    embedBuilder.addField(new MessageEmbed.Field(command.getUsageHelp(), "You don\'t have permission to use this command", false));
                }

                messageBuilder.setEmbed(embedBuilder.build());
                channel.sendMessage(messageBuilder.build()).queue();
            } else {
                for (CommandCategory commandCategory : LIST_COMMANDS) {
                    MessageBuilder messageBuilder = new MessageBuilder();
                    EmbedBuilder embedBuilder = new EmbedBuilder();
                    embedBuilder.setColor(Color.CYAN);
                    embedBuilder.setTitle(commandCategory.category);
                    boolean atLeastOneCommandIsAllowed = false;
                    for (BaseCommand command : commandCategory.map_commands.values()) {
                        if (!command.requiresPermission() || hasPermission) {
                            embedBuilder.addField(new MessageEmbed.Field(command.getUsageHelp(), command.getDescription(), false));
                            atLeastOneCommandIsAllowed = true;
                        }
                    }
                    if (!atLeastOneCommandIsAllowed) {
                        continue;
                    }
                    messageBuilder.setEmbed(embedBuilder.build());
                    channel.sendMessage(messageBuilder.build()).queue();
                }
            }
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }

    private static class CommandCategory {

        private final String category;
        private final Map<String, BaseCommand> map_commands = new LinkedHashMap<>();

        private CommandCategory(String category) {
            this.category = category;
        }
    }

}
