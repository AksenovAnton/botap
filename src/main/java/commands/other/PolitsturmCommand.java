package commands.other;

import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import commands.BaseCommand;

public class PolitsturmCommand extends BaseCommand {

	public static final String COMMAND = "Politsturm";
	private static final String DESCRIPTION = "Politsturmifies the text";
	private static final String USAGE_HELP = COMMAND + " <chance> <text>";

	private static final String SENTENCES_FILENAME = "Politsturm_sentences.txt";

	private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 2;

	private static final int INDEX_CHANCE = BaseCommand.MIN_COMMAND_LENGTH;

	private static List<String> sentencesToAdd = new ArrayList<>();

	private static final Object lock = new Object();

	static {
		try {
			BufferedReader reader = Files.newBufferedReader(FileSystems.getDefault().getPath(SENTENCES_FILENAME), StandardCharsets.UTF_8);

			String currentLine = reader.readLine();
			while (currentLine != null) {
				if (!currentLine.trim().isEmpty()) {
					sentencesToAdd.add(currentLine.trim());
				}
				currentLine = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public PolitsturmCommand(MessageReceivedEvent event, String[] commandData) {
		super(event, commandData);

		validate();
	}

	private void validate() {
		if (insufficientRights()) {
			return;
		}

		if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
			return;
		}

		try {
			int chance = Integer.parseInt(commandData[INDEX_CHANCE]);
			if (chance < 1 || chance > 100) {
				throw new NumberFormatException("Chance must be a number between 1 and 100");
			}
		} catch (NumberFormatException e) {
			invalidReason = "Chance must be a number between 1 and 100";
			isValid = false;
			return;
		}

		isValid = true;
	}

	@Override
	public void execute() {
		if (isValid) {
			synchronized (lock) {
				StringBuilder builder = new StringBuilder();
				for (int i = MIN_COMMAND_LENGTH - 1; i < commandData.length; i++) {
					builder.append(commandData[i]).append(" ");
				}

				List<String> responses = splitIfTooLarge(politsturmify(builder.toString(), Integer.parseInt(commandData[INDEX_CHANCE])));

				for (String response : responses) {
					MessageBuilder messageBuilder = new MessageBuilder();
					messageBuilder.appendCodeBlock(response, "");
					channel.sendMessage(messageBuilder.build()).queue();
				}
			}
		} else {
			showError(invalidReason);
		}
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getUsageHelp() {
		return USAGE_HELP;
	}

	@Override
	public boolean requiresPermission() {
		return true;
	}

	private String politsturmify(String input, int chance) {
		BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.forLanguageTag("ru"));
		iterator.setText(input);

		Random random = new Random();
		List<String> sentencesLeft = new ArrayList<>(sentencesToAdd);

		StringBuilder builder = new StringBuilder();
		int start = iterator.first();
		for (int end = iterator.next();
			 end != BreakIterator.DONE;
			 start = end, end = iterator.next()) {

			builder.append(input, start, end);
			if (random.nextInt(100) >= (100 - chance)) {
				if (sentencesLeft.isEmpty()) {
					sentencesLeft = new ArrayList<>(sentencesToAdd);
				}

				int index = random.nextInt(sentencesLeft.size());
				builder.append(sentencesLeft.get(index)).append(" ");
				sentencesLeft.remove(index);
			}
		}

		return builder.toString();
	}

	private List<String> splitIfTooLarge(String input) {
		int MESSAGE_LIMIT = 1900;

		List<String> output = new ArrayList<>();
		Pattern p = Pattern.compile("\\G\\s*(.{1," + MESSAGE_LIMIT + "})(?=\\s|$)", Pattern.DOTALL);
		Matcher m = p.matcher(input);
		while (m.find()) {
			output.add(m.group(1));
		}

		return output;
	}

}
