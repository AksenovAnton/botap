package commands.other;

import commands.BaseCommand;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.List;

public class DeleteCommand extends BaseCommand {

    public static final String COMMAND = "Delete";
    private static final String DESCRIPTION = "Deletes last messages in the channel";
    private static final String USAGE_HELP = COMMAND + " <amount>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

    private static final int INDEX_AMOUNT = BaseCommand.MIN_COMMAND_LENGTH;

    public DeleteCommand(MessageReceivedEvent event, String[] commandData) {
        super(event, commandData);

        validate();
    }

    private void validate() {
        if (insufficientRights()) {
            return;
        }

        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        try {
            int amount = Integer.parseInt(commandData[INDEX_AMOUNT]);
            if (amount <= 0 || amount > 100) {
                throw new NumberFormatException("Amount must be > 0 and <= 100");
            }
        } catch (NumberFormatException e) {
            invalidReason = "Please specify amount higher than 0";
            isValid = false;
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            int amount = Integer.parseInt(commandData[INDEX_AMOUNT]);
            List<Message> history = channel.getHistoryBefore(channel.getLatestMessageId(), amount).complete().getRetrievedHistory();
            if (amount > 1) {
            	try {
					channel.deleteMessages(history).complete();
				} catch (IllegalArgumentException e) {
            		e.printStackTrace();
					showError("Some of the messages are too old");
				}
            } else {
                channel.deleteMessageById(history.get(0).getId()).complete();
            }
            channel.deleteMessageById(commandMessage.getId()).submit();
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return true;
    }

}
