package commands.music;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.Objects;

import commands.BaseCommand;
import controllers.MusicController;

abstract class BaseMusicCommand extends BaseCommand {

	final MusicController musicController;

	BaseMusicCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
		super(event, commandData);

		this.musicController = musicController;
	}

	boolean wrongVoiceChannel(boolean requireActiveConnection) {
		if (requireActiveConnection) {
			if (musicController.getCurrentChannelId(member.getGuild()) == null) {
				invalidReason = "I am not connected to any voice channel";
				isValid = false;
				return true;
			}
		} else {
			if (!Objects.requireNonNull(member.getVoiceState()).inVoiceChannel()) {
				invalidReason = "You are not in a voice channel";
				isValid = false;
				return true;
			}
		}

		if (musicController.getCurrentChannelId(member.getGuild()) != null
				&& (!Objects.requireNonNull(member.getVoiceState()).inVoiceChannel()
				|| !Objects.requireNonNull(Objects.requireNonNull(member.getVoiceState()).getChannel()).getId().equals(musicController.getCurrentChannelId(member.getGuild())))) {
			invalidReason = "You must be in the same voice channel as me";
			isValid = false;
			return true;
		}

		return false;
	}

	protected static long timeStringToPosition(String timeString) {
		String[] values = timeString.split(":");
		long position = 0;
		for (int i = 0; i < values.length; i++) {
			int index = values.length - i - 1;
			if (i == 0) { // seconds
				position += Integer.parseInt(values[index]);
			} else if (i == 1) { // minutes
				position += Integer.parseInt(values[index]) * 60;
			} else if (i == 2) { // hours
				position += Integer.parseInt(values[index]) * 60 * 60;
			}
		}
		position *= 1000;
		if (position < 0) {
			position = 0;
		}

		return position;
	}

}
