package commands.music;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.Objects;

import commands.BaseCommand;
import controllers.MusicController;

public class PlayCommand extends BaseMusicCommand {

	public static final String COMMAND = "Play";
	private static final String DESCRIPTION = "Joins the voice channel you are in and plays the requested track";
	private static final String USAGE_HELP = COMMAND + " <url> [time]";

	private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

	private static final int INDEX_IDENTIFIER = BaseCommand.MIN_COMMAND_LENGTH;
	private static final int INDEX_POSITION = BaseCommand.MIN_COMMAND_LENGTH + 1;

	public PlayCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
		super(event, commandData, musicController);

		validate();
	}

	private void validate() {
		if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
			return;
		}

		if (wrongVoiceChannel(false)) {
			return;
		}

		if (commandData[INDEX_IDENTIFIER].contains("&list=")) {
			commandData[INDEX_IDENTIFIER] = commandData[INDEX_IDENTIFIER].substring(0, commandData[INDEX_IDENTIFIER].indexOf("&list="));
		}

		if (commandData.length > INDEX_POSITION) {
			String[] values = commandData[INDEX_POSITION].split(":");
			try {
				if (values.length > 3) {
					throw new NumberFormatException("Incorrect time format");
				}

				for (String value : values) {
					Integer.parseInt(value);
				}
			} catch (NumberFormatException e) {
				invalidReason = "Incorrect time. Please format the time like \"12:34:56\", \"34:56\" or \"56\"";
				isValid = false;
				return;
			}
		}

		isValid = true;
	}

	@Override
	public void execute() {
		if (isValid) {
			if (musicController.getCurrentChannelId(member.getGuild()) == null) {
				musicController.joinVoiceChannel(Objects.requireNonNull(Objects.requireNonNull(member.getVoiceState()).getChannel()));
			}

			long position = 0;
			if (commandData.length > INDEX_POSITION) {
				position = timeStringToPosition(commandData[INDEX_POSITION]);
			}

			musicController.addTrackToQueue(member.getGuild(), channel, commandData[INDEX_IDENTIFIER], position);
		} else {
			showError(invalidReason);
		}
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getUsageHelp() {
		return USAGE_HELP;
	}

	@Override
	public boolean requiresPermission() {
		return false;
	}

}
