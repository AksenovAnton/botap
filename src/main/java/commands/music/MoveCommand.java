package commands.music;

import commands.BaseCommand;
import controllers.MusicController;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class MoveCommand extends BaseMusicCommand {

	public static final String COMMAND = "Move";
	private static final String DESCRIPTION = "Moves the specified track to the top, making it next to current track";
	private static final String USAGE_HELP = COMMAND + " <position> [new position]";

	private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

	private static final int INDEX_POSITION = BaseCommand.MIN_COMMAND_LENGTH;
	private static final int INDEX_NEW_POSITION = BaseCommand.MIN_COMMAND_LENGTH + 1;

	public MoveCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
		super(event, commandData, musicController);

		validate();
	}

	private void validate() {
		if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
			return;
		}

		if (wrongVoiceChannel(true)) {
			return;
		}

		try {
			Integer.parseInt(commandData[INDEX_POSITION]);
		} catch (NumberFormatException e) {
			invalidReason = "Position must be a number";
			isValid = false;
			return;
		}

		if (commandData.length > MIN_COMMAND_LENGTH) {
			try {
				Integer.parseInt(commandData[INDEX_NEW_POSITION]);
			} catch (NumberFormatException e) {
				invalidReason = "New position must be a number";
				isValid = false;
				return;
			}
		}

		isValid = true;
	}

	@Override
	public void execute() {
		if (isValid) {
			if (commandData.length > MIN_COMMAND_LENGTH) {
				musicController.move(member.getGuild(), channel, Integer.parseInt(commandData[INDEX_POSITION]), Integer.parseInt(commandData[INDEX_NEW_POSITION]));
			} else {
				musicController.move(member.getGuild(), channel, Integer.parseInt(commandData[INDEX_POSITION]), 2);
			}
		} else {
			showError(invalidReason);
		}
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getUsageHelp() {
		return USAGE_HELP;
	}

	@Override
	public boolean requiresPermission() {
		return false;
	}

}
