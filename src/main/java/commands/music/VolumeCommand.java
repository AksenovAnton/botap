package commands.music;

import commands.BaseCommand;
import controllers.MusicController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class VolumeCommand extends BaseMusicCommand {

    public static final String COMMAND = "Volume";
    private static final String DESCRIPTION = "Sets or prints the player volume";
    private static final String USAGE_HELP = COMMAND + " [volume]";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    private static final int INDEX_VOLUME = BaseCommand.MIN_COMMAND_LENGTH;

    public VolumeCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
        super(event, commandData, musicController);

        validate();
    }

    private void validate() {
        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        if (wrongVoiceChannel(true)) {
            return;
        }

        if (commandData.length > MIN_COMMAND_LENGTH) {
            try {
                int volume = Integer.parseInt(commandData[INDEX_VOLUME]);

                if (volume < 0 || volume > 1000) {
                    throw new IllegalArgumentException("Volume must be a number between 0 and 1000");
                }
            } catch (IllegalArgumentException e) {
                invalidReason = "Volume must be a number between 0 and 1000";
                isValid = false;
                return;
            }
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            if (commandData.length > MIN_COMMAND_LENGTH) {
                musicController.setVolume(member.getGuild(), channel, Integer.parseInt(commandData[INDEX_VOLUME]));
            } else {
                musicController.printVolume(member.getGuild(), channel);
            }
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }

}
