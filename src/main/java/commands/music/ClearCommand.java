package commands.music;

import commands.BaseCommand;
import controllers.MusicController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class ClearCommand extends BaseMusicCommand {

    public static final String COMMAND = "Clear";
    private static final String DESCRIPTION = "Stops current track and clears the queue";
    private static final String USAGE_HELP = COMMAND;

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    public ClearCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
        super(event, commandData, musicController);

        validate();
    }

    private void validate() {
        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        if (wrongVoiceChannel(true)) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            musicController.clear(member.getGuild(), channel);
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }

}
