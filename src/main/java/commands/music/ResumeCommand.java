package commands.music;

import commands.BaseCommand;
import controllers.MusicController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class ResumeCommand extends BaseMusicCommand {

    public static final String COMMAND = "Resume";
    private static final String DESCRIPTION = "Continues playing audio";
    private static final String USAGE_HELP = COMMAND;

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH;

    public ResumeCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
        super(event, commandData, musicController);

        validate();
    }

    private void validate() {
        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        if (wrongVoiceChannel(true)) {
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            musicController.resume(member.getGuild(), channel);
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }

}
