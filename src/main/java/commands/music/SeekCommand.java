package commands.music;

import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import commands.BaseCommand;
import controllers.MusicController;

public class SeekCommand extends BaseMusicCommand {

	public static final String COMMAND = "Seek";
	private static final String DESCRIPTION = "Shifts current track to a new position";
	private static final String USAGE_HELP = COMMAND + " <time>";

	private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

	private static final int INDEX_POSITION = BaseCommand.MIN_COMMAND_LENGTH;

	public SeekCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
		super(event, commandData, musicController);

		validate();
	}

	private void validate() {
		if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
			return;
		}

		if (wrongVoiceChannel(false)) {
			return;
		}

		String[] values = commandData[INDEX_POSITION].split(":");
		try {
			if (values.length > 3) {
				throw new NumberFormatException("Incorrect time format");
			}

			for (String value : values) {
				Integer.parseInt(value);
			}
		} catch (NumberFormatException e) {
			invalidReason = "Incorrect time. Please format the time like \"12:34:56\", \"34:56\" or \"56\"";
			isValid = false;
			return;
		}

		isValid = true;
	}

	@Override
	public void execute() {
		if (isValid) {
			long position = 0;
			if (commandData.length > INDEX_POSITION) {
				position = timeStringToPosition(commandData[INDEX_POSITION]);
			}

			musicController.seek(member.getGuild(), channel, position);
		} else {
			showError(invalidReason);
		}
	}

	@Override
	public String getCommand() {
		return COMMAND;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getUsageHelp() {
		return USAGE_HELP;
	}

	@Override
	public boolean requiresPermission() {
		return false;
	}

}
