package commands.music;

import commands.BaseCommand;
import controllers.MusicController;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class RemoveCommand extends BaseMusicCommand {

    public static final String COMMAND = "Remove";
    private static final String DESCRIPTION = "Removes the specified track from queue";
    private static final String USAGE_HELP = COMMAND + " <number>";

    private static final int MIN_COMMAND_LENGTH = BaseCommand.MIN_COMMAND_LENGTH + 1;

    private static final int INDEX_INDEX = BaseCommand.MIN_COMMAND_LENGTH;

    public RemoveCommand(MessageReceivedEvent event, String[] commandData, MusicController musicController) {
        super(event, commandData, musicController);

        validate();
    }

    private void validate() {
        if (wrongCommandLength(MIN_COMMAND_LENGTH)) {
            return;
        }

        if (wrongVoiceChannel(true)) {
            return;
        }

        try {
            Integer.parseInt(commandData[INDEX_INDEX]);
        } catch (NumberFormatException e) {
            invalidReason = "Index must be a number";
            isValid = false;
            return;
        }

        isValid = true;
    }

    @Override
    public void execute() {
        if (isValid) {
            musicController.remove(member.getGuild(), channel, Integer.parseInt(commandData[INDEX_INDEX]));
        } else {
            showError(invalidReason);
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getUsageHelp() {
        return USAGE_HELP;
    }

    @Override
    public boolean requiresPermission() {
        return false;
    }

}
