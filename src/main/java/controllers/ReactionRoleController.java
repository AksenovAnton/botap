package controllers;

import com.google.gson.Gson;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.message.MessageDeleteEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;

import java.io.*;
import java.util.*;

public class ReactionRoleController extends BaseController {

	private static final String CONFIG_FILENAME = "ReactionRolesCfg.json";

	public static final String TYPE_SINGLE = "single";
	public static final String TYPE_MULTI = "multi";

	private Config config;

	public ReactionRoleController(JDA jda) {
		super(jda);

		try {
			BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILENAME));
			StringBuilder builder = new StringBuilder();
			String currentLine = reader.readLine();
			while (currentLine != null) {
				builder.append(currentLine);
				builder.append("\n");
				currentLine = reader.readLine();
			}
			reader.close();

			config = new Gson().fromJson(builder.toString(), Config.class);
		} catch (IOException e) {
			config = new Config();
		}

		clearLostConfiguration();
	}

	private void saveConfiguration() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(CONFIG_FILENAME));
			writer.write(escapeJavaStyleString(new Gson().toJson(config)).replace("\\\"", "\""));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void clearLostConfiguration() {
		List<String> channelsToRemove = new ArrayList<>();
		for (String channelId : config.channels.keySet()) {
			if (jda.getTextChannelById(channelId) == null) {
				channelsToRemove.add(channelId);
				continue;
			}

			List<String> messagesToRemove = new ArrayList<>();
			for (String messageId : config.channels.get(channelId).messages.keySet()) {
				try {
					Objects.requireNonNull(jda.getTextChannelById(channelId)).retrieveMessageById(messageId).complete();
				} catch (ErrorResponseException e) {
					messagesToRemove.add(messageId);
				}
			}

			for (String messageId : messagesToRemove) {
				System.out.println("[ReactionRoleController][clearLostConfiguration] - Removing message " + messageId + " from channel " + channelId);
				config.channels.get(channelId).messages.remove(messageId);
			}
		}

		for (String channelId : channelsToRemove) {
			System.out.println("[ReactionRoleController][clearLostConfiguration] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}

		saveConfiguration();
	}

	public void setToggleType(String textChannelId, String messageId, String type) {
		if (!config.channels.containsKey(textChannelId)) {
			config.channels.put(textChannelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			channel.messages.put(messageId, new MessageConfig());
		}

		MessageConfig message = channel.messages.get(messageId);
		message.type = type.toLowerCase();

		saveConfiguration();
	}

	public void addToggleReaction(String textChannelId, String messageId, String emote, String role) {
		if (!config.channels.containsKey(textChannelId)) {
			config.channels.put(textChannelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			channel.messages.put(messageId, new MessageConfig());
		}

		MessageConfig message = channel.messages.get(messageId);
		message.roles.put(emote, role);

		saveConfiguration();
	}

	public void removeReaction(String textChannelId, String messageId, String emote) {
		if (!config.channels.containsKey(textChannelId)) {
			config.channels.put(textChannelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			channel.messages.put(messageId, new MessageConfig());
		}

		MessageConfig message = channel.messages.get(messageId);
		message.roles.remove(emote);

		saveConfiguration();
	}

	public void setDefaultRole(String textChannelId, String messageId, String role) {
		if (!config.channels.containsKey(textChannelId)) {
			config.channels.put(textChannelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			channel.messages.put(messageId, new MessageConfig());
		}

		MessageConfig message = channel.messages.get(messageId);
		message.defaultRole = role;

		saveConfiguration();
	}

	public void removeDefaultRole(String textChannelId, String messageId) {
		if (!config.channels.containsKey(textChannelId)) {
			config.channels.put(textChannelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			channel.messages.put(messageId, new MessageConfig());
		}

		MessageConfig message = channel.messages.get(messageId);
		message.defaultRole = null;

		saveConfiguration();
	}

	public void onGuildLeave(GuildLeaveEvent event) {
		List<String> channelsToRemove = new ArrayList<>();
		for (String channelId : config.channels.keySet()) {
			TextChannel textChannel = jda.getTextChannelById(channelId);
			if (textChannel != null) {
				if (textChannel.getGuild().getId().equals(event.getGuild().getId())) {
					channelsToRemove.add(channelId);
				}
			}
		}

		for (String channelId : channelsToRemove) {
			System.out.println("[ReactionRoleController][onGuildLeave] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}

		saveConfiguration();
	}

	public void onChannelDelete(TextChannelDeleteEvent event) {
		String channelId = event.getChannel().getId();
		if (config.channels.containsKey(channelId)) {
			System.out.println("[ReactionRoleController][onChannelDelete] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}

		saveConfiguration();
	}

	public void onMessageDelete(MessageDeleteEvent event) {
		String channelId = event.getChannel().getId();
		if (!config.channels.containsKey(channelId)) {
			return;
		}

		String messageId = event.getMessageId();
		if (config.channels.get(channelId).messages.containsKey(messageId)) {
			System.out.println("[ReactionRoleController][onMessageDelete] - Removing message " + messageId + " from channel " + channelId);
			config.channels.get(channelId).messages.remove(messageId);
		}

		saveConfiguration();
	}

	public void onReaction(MessageReactionAddEvent event) {
		String textChannelId = event.getTextChannel().getId();
		if (!config.channels.containsKey(textChannelId)) {
			return;
		}

		String messageId = event.getMessageId();
		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			return;
		}

		String emoteName = event.getReactionEmote().isEmoji()
				? event.getReactionEmote().getEmoji()
				: event.getReactionEmote().getName() + ":" + event.getReactionEmote().getIdLong();
		if (!channel.messages.get(messageId).roles.containsKey(emoteName)) {
			return;
		}

		MessageConfig message = channel.messages.get(messageId);
		if (!message.roles.containsKey(emoteName)) {
			return;
		}

		Role roleToGive = jda.getRoleById(message.roles.get(emoteName));
		assert roleToGive != null;

		if (!message.lastUserReacts.containsKey(event.getUserId())) {
			message.lastUserReacts.put(event.getUserId(), new LinkedList<>());
		}
		Queue<String> lastUserReacts = message.lastUserReacts.get(event.getUserId());

		if (message.type.equals(TYPE_SINGLE)) {
            /*for (Role role : Objects.requireNonNull(event.getMember()).getRoles()) {
                for (String key : message.roles.keySet()) {
                    if (!key.equals(emoteName) && message.roles.get(key).equals(role.getId())) {
                        event.getGuild().removeRoleFromMember(event.getMember(), role).submit();
                        event.getTextChannel().removeReactionById(messageId, key, Objects.requireNonNull(event.getUser())).submit();

                        lastUserReacts.remove(key);
                    }
                }

                if (role.getId().equals(message.defaultRole)) {
                    event.getGuild().removeRoleFromMember(event.getMember(), role).submit();
                }
            }*/
			assert event.getMember() != null;

			if (message.defaultRole != null && lastUserReacts.isEmpty()) {
				event.getGuild().removeRoleFromMember(event.getMember(), Objects.requireNonNull(jda.getRoleById(message.defaultRole))).submit();
			}

			for (String reactToRemove : lastUserReacts) {
				if (!reactToRemove.equals(emoteName)) {
					event.getTextChannel().removeReactionById(messageId, reactToRemove, Objects.requireNonNull(event.getUser())).submit();
					event.getGuild().removeRoleFromMember(event.getMember(), Objects.requireNonNull(jda.getRoleById(message.roles.get(reactToRemove)))).submit();
					lastUserReacts.remove(reactToRemove);
				}
			}
		}

		lastUserReacts.add(emoteName);

		event.getGuild().addRoleToMember(Objects.requireNonNull(event.getMember()), roleToGive).submit();

		saveConfiguration();
	}

	public void onReactionRemove(MessageReactionRemoveEvent event) {
		String textChannelId = event.getTextChannel().getId();
		if (!config.channels.containsKey(textChannelId)) {
			return;
		}

		String messageId = event.getMessageId();
		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			return;
		}

		String emoteName = event.getReactionEmote().isEmoji()
				? event.getReactionEmote().getEmoji()
				: event.getReactionEmote().getName() + ":" + event.getReactionEmote().getIdLong();
		if (!channel.messages.get(messageId).roles.containsKey(emoteName)) {
			return;
		}

		MessageConfig message = channel.messages.get(messageId);
		if (!message.roles.containsKey(emoteName)) {
			return;
		}

		Queue<String> lastUserReacts = message.lastUserReacts.get(event.getUserId());
		if (lastUserReacts.contains(emoteName)) {
			Role roleToRemove = jda.getRoleById(message.roles.get(emoteName));
			assert roleToRemove != null;
			event.getGuild().removeRoleFromMember(Objects.requireNonNull(event.getMember()), roleToRemove).submit();

			lastUserReacts.remove(emoteName);

			if (message.defaultRole != null && lastUserReacts.isEmpty()) {
				Role roleToGive = jda.getRoleById(message.defaultRole);
				assert roleToGive != null;
				event.getGuild().addRoleToMember(event.getMember(), roleToGive).submit();
			}

			saveConfiguration();
		}
	}

	private static class Config {
		private final Map<String, ChannelConfig> channels = new HashMap<>();
	}

	private static class ChannelConfig {
		private final Map<String, MessageConfig> messages = new HashMap<>();
	}

	private static class MessageConfig {
		private String type = TYPE_MULTI;
		private String defaultRole = null;
		private final Map<String, String> roles = new HashMap<>();

		private final Map<String, Queue<String>> lastUserReacts = new HashMap<>();
	}

}
