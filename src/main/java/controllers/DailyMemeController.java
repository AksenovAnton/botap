package controllers;

import com.google.gson.Gson;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.TextChannel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class DailyMemeController extends BaseController {

	private static final String CONFIG_FILENAME = "DailyMemeCfg.json";

	public static final String IMAGES_FOLDER = "DailyMemes";
	private static final String ALREADY_SENT_FOLDER = "AlreadySent";

	private static final int CORE_POOL = 2;
	private static final long EVERY_DAY_DELAY = 24 * 60 * 60 * 1000;

	private Config config;

	private ScheduledExecutorService scheduler;

	private Map<String, ScheduledFuture<?>> map_schedules = new HashMap<>();

	public DailyMemeController(JDA jda) {
		super(jda);

		try {
			BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILENAME));
			StringBuilder builder = new StringBuilder();
			String currentLine = reader.readLine();
			while (currentLine != null) {
				builder.append(currentLine);
				builder.append("\n");
				currentLine = reader.readLine();
			}
			reader.close();

			config = new Gson().fromJson(builder.toString(), Config.class);
		} catch (IOException e) {
			config = new Config();
		}

		clearLostConfiguration();

		scheduler = Executors.newScheduledThreadPool(CORE_POOL);
		for (String channelId : config.channels.keySet()) {
			TextChannel textChannel = jda.getTextChannelById(channelId);
			if (textChannel != null) {
				ChannelConfig channelConfig = config.channels.get(channelId);

				channelConfig.message = decode(channelConfig.message).replace("\\", "");

				startSchedule(textChannel, channelConfig.folderName, channelConfig.hour, channelConfig.message);
			}
		}

		saveConfiguration();
	}

	private void saveConfiguration() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(CONFIG_FILENAME));
			writer.write(escapeJavaStyleString(new Gson().toJson(config)).replace("\\\"", "\""));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void clearLostConfiguration() {
		List<String> channelsToRemove = new ArrayList<>();
		for (String channelId : config.channels.keySet()) {
			if (jda.getTextChannelById(channelId) == null) {
				channelsToRemove.add(channelId);
			}
		}

		for (String channelId : channelsToRemove) {
			System.out.println("[DailyMemeController][clearLostConfiguration] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}
	}

	private void startSchedule(TextChannel textChannel, String folderName, int hour, String message) {
		ScheduledFuture<?> future = map_schedules.get(textChannel.getId());
		if (future != null) {
			future.cancel(true);
		}

		Runnable runnable = () -> {
			File folder = new File(IMAGES_FOLDER, folderName);
			File sentFilesFolder = new File(IMAGES_FOLDER + "/" + folderName, ALREADY_SENT_FOLDER);

			if (folder.exists() && folder.isDirectory()) {
				File[] files = folder.listFiles(file -> !file.isDirectory());
				assert files != null;

				if (files.length > 0) {
					int index = new Random().nextInt(files.length);
					File fileToSend = files[index];

					sendFile(textChannel, fileToSend, message);

					File renameToFile = new File(sentFilesFolder, fileToSend.getName());
					if (!sentFilesFolder.exists()) {
						sentFilesFolder.mkdirs();
					}
					fileToSend.renameTo(renameToFile);
				} else {
					File[] sentFiles = sentFilesFolder.listFiles(file -> !file.isDirectory());
					assert sentFiles != null;

					for (File file : sentFiles) {
						file.renameTo(new File(folder, file.getName()));
					}

					files = folder.listFiles(file -> !file.isDirectory());
					assert files != null;

					int index = new Random().nextInt(files.length);
					File fileToSend = files[index];
					sendFile(textChannel, fileToSend, message);

					File renameToFile = new File(sentFilesFolder, fileToSend.getName());
					if (!sentFilesFolder.exists()) {
						sentFilesFolder.mkdirs();
					}
					fileToSend.renameTo(renameToFile);
				}
			} else {
				stopSchedule(textChannel);
			}
		};

		Calendar now = Calendar.getInstance();
		Calendar next = Calendar.getInstance();
		next.set(Calendar.HOUR_OF_DAY, hour);
		next.set(Calendar.MINUTE, 0);
		next.set(Calendar.SECOND, 0);
		next.set(Calendar.MILLISECOND, 0);
		if (next.before(now)) {
			next.add(Calendar.DATE, 1);
		}

		future = scheduler.scheduleAtFixedRate(runnable, next.getTimeInMillis() - now.getTimeInMillis(), EVERY_DAY_DELAY, TimeUnit.MILLISECONDS);
		map_schedules.put(textChannel.getId(), future);
	}

	private void stopSchedule(TextChannel textChannel) {
		map_schedules.get(textChannel.getId()).cancel(true);
		map_schedules.remove(textChannel.getId());
	}

	public void startMemes(String channelId, String folderName, int hour, String message) {
		if (!config.channels.containsKey(channelId)) {
			config.channels.put(channelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(channelId);
		channel.folderName = folderName;
		channel.hour = hour;
		channel.message = message;

		saveConfiguration();

		startSchedule(Objects.requireNonNull(jda.getTextChannelById(channelId)), folderName, hour, message);
	}

	public void stopMemes(String channelId) {
		if (config.channels.containsKey(channelId)) {
			config.channels.remove(channelId);

			saveConfiguration();

			stopSchedule(Objects.requireNonNull(jda.getTextChannelById(channelId)));
		}
	}

	private void sendFile(TextChannel textChannel, File fileToSend, String message) {
		if (fileToSend.getPath().contains(".txt")) {
			try {
				String text = readFile(fileToSend);
				textChannel.sendMessage(message + "\n" + text).submit();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			textChannel.sendMessage(message).addFile(fileToSend, fileToSend.getName()).complete();
		}
	}

	private static String decode(String in) {
		String working = in;
		int index;
		index = working.indexOf("\\u");
		while (index > -1) {
			int length = working.length();
			if (index > (length - 6)) break;
			int numStart = index + 2;
			int numFinish = numStart + 4;
			String substring = working.substring(numStart, numFinish);
			int number = Integer.parseInt(substring, 16);
			String stringStart = working.substring(0, index);
			String stringEnd = working.substring(numFinish);
			working = stringStart + ((char) number) + stringEnd;
			index = working.indexOf("\\u");
		}
		return working;
	}

	private static String readFile(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
		StringBuilder builder = new StringBuilder();
		String currentLine = reader.readLine();
		while (currentLine != null) {
			builder.append(currentLine);
			builder.append("\n");
			currentLine = reader.readLine();
		}
		reader.close();

		return builder.toString();
	}

	private static class Config {
		private final Map<String, ChannelConfig> channels = new HashMap<>();
	}

	private static class ChannelConfig {
		private String folderName;
		private String message;
		private int hour;
	}

}
