package controllers;

import com.google.gson.Gson;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.audio.AudioSendHandler;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

public class MusicController extends BaseController {

	private static final String CONFIG_FILENAME = "MusicCfg.json";

	private static final long AUTO_DISCONNECT_DELAY = 5; // 5 minutes

	private final AudioPlayerManager playerManager;

	private final Map<String, MusicPlayer> map_musicPlayers = new HashMap<>();

	private Config config;

	public MusicController(JDA jda) {
		super(jda);

		try {
			BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILENAME));
			StringBuilder builder = new StringBuilder();
			String currentLine = reader.readLine();
			while (currentLine != null) {
				builder.append(currentLine);
				builder.append("\n");
				currentLine = reader.readLine();
			}
			reader.close();

			config = new Gson().fromJson(builder.toString(), Config.class);
		} catch (IOException e) {
			config = new Config();
		}

		clearLostConfiguration();

		playerManager = new DefaultAudioPlayerManager();
		AudioSourceManagers.registerRemoteSources(playerManager);
	}

	private void saveConfiguration() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(CONFIG_FILENAME));
			writer.write(escapeJavaStyleString(new Gson().toJson(config)).replace("\\\"", "\""));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void clearLostConfiguration() {
		List<String> serversToRemove = new ArrayList<>();
		for (String serverId : config.servers.keySet()) {
			if (jda.getGuildById(serverId) == null) {
				serversToRemove.add(serverId);
			}
		}

		for (String serverId : serversToRemove) {
			System.out.println("[MusicController][clearLostConfiguration] - Removing server " + serverId);
			config.servers.remove(serverId);
		}

		saveConfiguration();
	}

	public void onGuildLeave(GuildLeaveEvent event) {
		String serverId = event.getGuild().getId();
		if (config.servers.containsKey(serverId)) {
			System.out.println("[NewMemberController][onGuildLeave] - Removing server " + serverId);
			config.servers.remove(serverId);
		}

		saveConfiguration();
	}

	public void joinVoiceChannel(VoiceChannel voiceChannel) {
		AudioManager audioManager = voiceChannel.getGuild().getAudioManager();
		audioManager.setAutoReconnect(true);

		ServerConfig serverConfig;
		if (config.servers.containsKey(voiceChannel.getGuild().getId())) {
			serverConfig = config.servers.get(voiceChannel.getGuild().getId());
		} else {
			serverConfig = new ServerConfig();
			config.servers.put(voiceChannel.getGuild().getId(), serverConfig);
			saveConfiguration();
		}

		MusicPlayer musicPlayer = new MusicPlayer(voiceChannel.getGuild(), playerManager, serverConfig,
				musicPlayer1 -> {
					if (audioManager.isConnected()) {
						leaveVoiceChannel(musicPlayer1.guild, musicPlayer1.lastChannel);
					}
				});
		audioManager.setSendingHandler(musicPlayer);
		if (!audioManager.isConnected()) {
			audioManager.openAudioConnection(voiceChannel);
		}

		map_musicPlayers.put(voiceChannel.getGuild().

				getId(), musicPlayer);
	}

	public void leaveVoiceChannel(Guild guild, MessageChannel channel) {
		AudioManager audioManager = guild.getAudioManager();
		if (audioManager.isConnected()) {
			audioManager.closeAudioConnection();
			MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
			musicPlayer.destroy();
			map_musicPlayers.remove(guild.getId());
			sendMessage(channel, Color.GREEN, new String[][]{
					{"Disconnected", ""}});
		} else {
			sendMessage(channel, Color.RED, new String[][]{
					{"I am not connected to any channel", ""}
			});
		}
	}

	public void addTrackToQueue(Guild guild, MessageChannel channel, String url, long position) {
		playerManager.loadItem(url, new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {
				System.out.println("[MusicController][addTrackToQueue] - trackLoaded");
				map_musicPlayers.get(guild.getId()).lastChannel = channel;
				if (track.isSeekable()) {
					track.setPosition(position);
				}
				map_musicPlayers.get(guild.getId()).queue(track);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				System.out.println("[MusicController][addTrackToQueue] - playlistLoaded");
				map_musicPlayers.get(guild.getId()).lastChannel = channel;
				if (playlist.getSelectedTrack().isSeekable()) {
					playlist.getSelectedTrack().setPosition(position);
				}
				map_musicPlayers.get(guild.getId()).queue(playlist.getSelectedTrack());
                /*for (AudioTrack audioTrack : playlist.getTracks()) {
                    map_musicPlayers.get(guild.getId()).
                }*/
			}

			@Override
			public void noMatches() {
				System.out.println("[MusicController][addTrackToQueue] - onMatches");
				sendMessage(channel, Color.RED, new String[][]{
						{"Can't find the track", url}
				});
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				System.out.println("[MusicController][addTrackToQueue] - loadFailed");
				sendMessage(channel, Color.RED, new String[][]{
						{exception.getMessage(), url}});
			}
		});
	}

	public void pause(Guild guild, MessageChannel channel) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.pause();

		if (musicPlayer.getQueue().isEmpty()) {
			sendMessage(channel, Color.RED, new String[][]{
					{"Playing", "Nothing to skip"}
			});
		} else {
			AudioTrack track = musicPlayer.getQueue().get(0);
			sendMessage(channel, Color.GREEN, new String[][]{
					{"Paused", trackToTrackName(track)}
			});
		}
	}

	public void resume(Guild guild, MessageChannel channel) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		if (musicPlayer.isPaused()) {
			AudioTrack track = musicPlayer.getQueue().get(0);
			if (track != null) {
				musicPlayer.resume();
				sendMessage(channel, Color.GREEN, new String[][]{
						{"Resuming", trackToTrackName(track)}
				});
			} else {
				sendMessage(channel, Color.RED, new String[][]{
						{"Audio queue is empty", ""}
				});
			}
		} else {
			sendMessage(channel, Color.RED, new String[][]{
					{"Player is not paused", ""}
			});
		}
	}

	public void skip(Guild guild, MessageChannel channel) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.skip();
	}

	public void move(Guild guild, MessageChannel channel, int position, int newPosition) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.move(position, newPosition);
	}

	public void remove(Guild guild, MessageChannel channel, int index) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.remove(index);
	}

	public void clear(Guild guild, MessageChannel channel) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.clear();
		sendMessage(channel, Color.GREEN, new String[][]{
				{"Cleared the queue", ""}
		});
	}

	public void seek(Guild guild, MessageChannel channel, long newPosition) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.seek(newPosition);
	}

	public void printQueue(Guild guild, MessageChannel channel) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;

		if (!musicPlayer.getQueue().isEmpty()) {
			String[][] queueData = new String[musicPlayer.getQueue().size()][];
			for (int i = 0; i < musicPlayer.getQueue().size(); i++) {
				if (i == 0) {
					queueData[i] = new String[]{"Playing", trackToTrackName(musicPlayer.getQueue().get(i))};
				} else {
					queueData[i] = new String[]{String.valueOf(i), trackToTrackName(musicPlayer.getQueue().get(i))};
				}
			}
			sendMessage(channel, Color.CYAN, queueData);
		} else {
			sendMessage(channel, Color.CYAN, new String[][]{
					{"Queue is empty", ""}
			});
		}
	}

	public void setVolume(Guild guild, MessageChannel channel, int volume) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		musicPlayer.setVolume(volume);
		sendMessage(channel, Color.GREEN, new String[][]{
				{"Volume set to " + volume, ""}
		});

		config.servers.get(guild.getId()).volume = volume;
		saveConfiguration();
	}

	public void printVolume(Guild guild, MessageChannel channel) {
		MusicPlayer musicPlayer = map_musicPlayers.get(guild.getId());
		musicPlayer.lastChannel = channel;
		sendMessage(channel, Color.GREEN, new String[][]{
				{"Current volume is " + musicPlayer.getVolume(), ""}
		});
	}

	public String getCurrentChannelId(Guild guild) {
		if (guild.getAudioManager().isConnected()) {
			return Objects.requireNonNull(guild.getAudioManager().getConnectedChannel()).getId();
		} else {
			return null;
		}
	}

	private static void sendMessage(MessageChannel channel, Color messageColor, String[][] messages) {
		MessageBuilder messageBuilder = new MessageBuilder();
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setColor(messageColor);
		for (String[] message : messages) {
			embedBuilder.addField(new MessageEmbed.Field(message[0], message[1], false));
		}
		messageBuilder.setEmbed(embedBuilder.build());
		channel.sendMessage(messageBuilder.build()).queue();
	}

	private static String trackToTrackName(AudioTrack track) {
		return track.getInfo().author + " - " + track.getInfo().title + " - " + positionToTimeString(track.getPosition());
	}

	private static class MusicPlayer extends AudioEventAdapter implements AudioSendHandler {

		private Guild guild;
		private MessageChannel lastChannel;
		private final AudioPlayer audioPlayer;
		private final List<AudioTrack> queue = new LinkedList<>();

		private ScheduledExecutorService scheduler;
		private ScheduledFuture<?> scheduledFuture;

		private OnEventListener onEventListener;

		private AudioFrame lastFrame;

		private MusicPlayer(Guild guild, AudioPlayerManager playerManager, ServerConfig serverConfig, OnEventListener onEventListener) {
			this.guild = guild;
			this.audioPlayer = playerManager.createPlayer();
			this.onEventListener = onEventListener;
			audioPlayer.addListener(this);
			audioPlayer.setVolume(serverConfig.volume);

			scheduler = Executors.newScheduledThreadPool(1);
		}

		private void queue(AudioTrack track) {
			queue.add(track);
			if (queue.size() == 1) {
				audioPlayer.playTrack(track);
				sendMessage(lastChannel, Color.GREEN, new String[][]{
						{"Playing", trackToTrackName(track)}
				});
			} else {
				sendMessage(lastChannel, Color.GREEN, new String[][]{
						{"Added track to the queue", trackToTrackName(track)}
				});
			}
		}

		private List<AudioTrack> getQueue() {
			return queue;
		}

		private void skip() {
			if (!queue.isEmpty()) {
				AudioTrack currentTrack = queue.get(0);
				audioPlayer.stopTrack();
				if (!queue.isEmpty()) {
					AudioTrack nextTrack = queue.get(0);
					sendMessage(lastChannel, Color.GREEN, new String[][]{
							{"Skipped", trackToTrackName(currentTrack)},
							{"Playing", trackToTrackName(nextTrack)}
					});
					audioPlayer.playTrack(nextTrack);
				} else {
					sendMessage(lastChannel, Color.GREEN, new String[][]{
							{"Skipped", trackToTrackName(currentTrack)}
					});
				}
			} else {
				sendMessage(lastChannel, Color.RED, new String[][]{
						{"Playing", "Nothing to skip"}
				});
			}
		}

		private void move(int position, int newPosition) {
			if (position > 1 && position <= queue.size()) {
				if (newPosition > 0 && newPosition <= queue.size()) {
					queue.add(newPosition - 1, queue.remove(position - 1));
					sendMessage(lastChannel, Color.GREEN, new String[][]{
							{"Moved", trackToTrackName(queue.get(1))}
					});

					if (newPosition == 1) {
						AudioTrack nextTrack = queue.get(0);
						sendMessage(lastChannel, Color.GREEN, new String[][]{
								{"Playing", trackToTrackName(nextTrack)}
						});
						audioPlayer.playTrack(nextTrack);
					}
				} else {
					sendMessage(lastChannel, Color.RED, new String[][]{
							{"Wrong index", "New position must be between 1 and " + (queue.size())}
					});
				}
			} else {
				sendMessage(lastChannel, Color.RED, new String[][]{
						{"Wrong index", "Position must be between 2 and " + (queue.size())}
				});
			}
		}

		private void remove(int index) {
			if (index >= 0 && index <= queue.size()) {
				AudioTrack trackToRemove = queue.get(index);
				if (index == 0) {
					audioPlayer.stopTrack();
					if (!queue.isEmpty()) {
						AudioTrack nextTrack = queue.get(0);
						audioPlayer.playTrack(nextTrack);
					}
				} else {
					queue.remove(trackToRemove);
				}
				sendMessage(lastChannel, Color.GREEN, new String[][]{
						{"Removed", trackToTrackName(trackToRemove)}
				});
			} else {
				sendMessage(lastChannel, Color.RED, new String[][]{
						{"Wrong index", "Index must be between 0 and " + queue.size()}
				});
			}
		}

		private void clear() {
			audioPlayer.stopTrack();
			queue.clear();
		}

		private void pause() {
			audioPlayer.setPaused(true);
		}

		private void resume() {
			audioPlayer.setPaused(false);
		}

		private void seek(long newPosition) {
			AudioTrack audioTrack = audioPlayer.getPlayingTrack();
			if (audioTrack == null) {
				sendMessage(lastChannel, Color.RED, new String[][]{
						{"Nothing playing right now"}
				});
				return;
			}

			if (audioTrack.isSeekable()) {
				audioTrack.setPosition(newPosition);
				sendMessage(lastChannel, Color.GREEN, new String[][]{
						{"Moved to new position", trackToTrackName(audioTrack)}
				});
			} else {
				sendMessage(lastChannel, Color.RED, new String[][]{
						{"Can't seek on this track", trackToTrackName(audioTrack)}
				});
			}
		}

		private boolean isPaused() {
			return audioPlayer.isPaused();
		}

		private void setVolume(int volume) {
			audioPlayer.setVolume(volume);
		}

		private int getVolume() {
			return audioPlayer.getVolume();
		}

		private void destroy() {
			clear();
			audioPlayer.destroy();
			if (scheduledFuture != null) {
				scheduledFuture.cancel(true);
			}
		}

		@Override
		public void onPlayerPause(AudioPlayer player) {
			System.out.println("[MusicController][MusicPlayer] - onPlayerPause");
		}

		@Override
		public void onPlayerResume(AudioPlayer player) {
			System.out.println("[MusicController][MusicPlayer] - onPlayerResume");
		}

		@Override
		public void onTrackStart(AudioPlayer player, AudioTrack track) {
			System.out.println("[MusicController][MusicPlayer] - onTrackStart");
			if (scheduledFuture != null) {
				scheduledFuture.cancel(true);
				scheduledFuture = null;
			}
		}

		@Override
		public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
			System.out.println("[MusicController][MusicPlayer] - onTrackEnd");

			queue.remove(track);

            /*if (endReason == AudioTrackEndReason.LOAD_FAILED) {
                sendMessage(lastChannel, Color.RED, new String[][]{
                        {"Failed to load the track", trackToTrackName(track)}
                });
            }*/

			if (endReason.mayStartNext) {
				if (queue.size() > 0) {
					audioPlayer.playTrack(queue.get(0));
				}
			}

			if (queue.isEmpty()) {
				scheduledFuture = scheduler.schedule(() -> {
					if (queue.isEmpty()) {
						onEventListener.wantToDisconnect(MusicPlayer.this);
					}
				}, AUTO_DISCONNECT_DELAY, TimeUnit.MINUTES);
			}
		}

		@Override
		public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {
			System.out.println("[MusicController][MusicPlayer] - onTrackException");
			sendMessage(lastChannel, Color.RED, new String[][]{
					{exception.getMessage(), trackToTrackName(track)}
			});
		}

		@Override
		public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs) {
			System.out.println("[MusicController][MusicPlayer] - onTrackStuck");
			sendMessage(lastChannel, Color.RED, new String[][]{
					{"Track is stuck", trackToTrackName(track)}
			});
		}

		@Override
		public boolean canProvide() {
			lastFrame = audioPlayer.provide();
			return lastFrame != null;
		}

		@Nullable
		@Override
		public ByteBuffer provide20MsAudio() {
			return ByteBuffer.wrap(lastFrame.getData());
		}

		@Override
		public boolean isOpus() {
			return true;
		}

		private interface OnEventListener {
			void wantToDisconnect(MusicPlayer musicPlayer);
		}
	}

	private static class Config {
		private final Map<String, ServerConfig> servers = new HashMap<>();
	}

	private static class ServerConfig {
		private int volume = 100;
	}

	private static String positionToTimeString(long position) {
		Date date = new Date(position);
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(date);
	}

}
