package controllers;

import com.google.gson.Gson;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;

import java.io.*;
import java.util.*;

public class NewMemberController extends BaseController {

    private static final String CONFIG_FILENAME = "NewMemberCfg.json";

    private Config config;

    public NewMemberController(JDA jda) {
        super(jda);

        try {
            BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILENAME));
            StringBuilder builder = new StringBuilder();
            String currentLine = reader.readLine();
            while (currentLine != null) {
                builder.append(currentLine);
                builder.append("\n");
                currentLine = reader.readLine();
            }
            reader.close();

            config = new Gson().fromJson(builder.toString(), Config.class);
        } catch (IOException e) {
            config = new Config();
        }

        clearLostConfiguration();
    }

    private void saveConfiguration() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(CONFIG_FILENAME));
            writer.write(escapeJavaStyleString(new Gson().toJson(config)).replace("\\\"", "\""));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clearLostConfiguration() {
        List<String> serversToRemove = new ArrayList<>();
        for (String serverId : config.servers.keySet()) {
            if (jda.getGuildById(serverId) == null) {
                serversToRemove.add(serverId);
            }
        }

        for (String serverId : serversToRemove) {
            System.out.println("[NewMemberController][clearLostConfiguration] - Removing server " + serverId);
            config.servers.remove(serverId);
        }

        saveConfiguration();
    }

    public void setNewMemberRoles(String serverId, String[] roles) {
        if (!config.servers.containsKey(serverId)) {
            config.servers.put(serverId, new ServerConfig());
        }

        ServerConfig server = config.servers.get(serverId);
        if (roles != null) {
            server.newMemberRoles.addAll(Arrays.asList(roles));
        } else {
            server.newMemberRoles.clear();
        }

        saveConfiguration();
    }

    public void onGuildLeave(GuildLeaveEvent event) {
        String serverId = event.getGuild().getId();
        if (config.servers.containsKey(serverId)) {
            System.out.println("[NewMemberController][onGuildLeave] - Removing server " + serverId);
            config.servers.remove(serverId);
        }

        saveConfiguration();
    }

    public void onNewMember(GuildMemberJoinEvent event) {
        if (!config.servers.containsKey(event.getGuild().getId())) {
            return;
        }

        ServerConfig server = config.servers.get(event.getGuild().getId());
        for (String roleName : server.newMemberRoles) {
            Role roleToGive = jda.getRoleById(roleName);
            assert roleToGive != null;
            event.getGuild().addRoleToMember(event.getMember(), roleToGive).submit();
        }
    }

    private static class Config {
        private final Map<String, ServerConfig> servers = new HashMap<>();
    }

    private static class ServerConfig {
        private final List<String> newMemberRoles = new ArrayList<>();
    }
}
