package controllers;

import com.google.gson.Gson;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.channel.text.TextChannelDeleteEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.message.MessageDeleteEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;

import java.io.*;
import java.util.*;

public class PollController extends BaseController {

	private static final String CONFIG_FILENAME = "PollsCfg.json";

	private Config config;

	public PollController(JDA jda) {
		super(jda);

		try {
			BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILENAME));
			StringBuilder builder = new StringBuilder();
			String currentLine = reader.readLine();
			while (currentLine != null) {
				builder.append(currentLine);
				builder.append("\n");
				currentLine = reader.readLine();
			}
			reader.close();

			config = new Gson().fromJson(builder.toString(), Config.class);
		} catch (IOException e) {
			config = new Config();
		}

		clearLostConfiguration();
	}

	private void saveConfiguration() {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(CONFIG_FILENAME));
			writer.write(escapeJavaStyleString(new Gson().toJson(config)).replace("\\\"", "\""));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void clearLostConfiguration() {
		List<String> channelsToRemove = new ArrayList<>();
		for (String channelId : config.channels.keySet()) {
			if (jda.getTextChannelById(channelId) == null) {
				channelsToRemove.add(channelId);
				continue;
			}

			List<String> messagesToRemove = new ArrayList<>();
			for (String messageId : config.channels.get(channelId).messages.keySet()) {
				try {
					Objects.requireNonNull(jda.getTextChannelById(channelId)).retrieveMessageById(messageId).complete();
				} catch (ErrorResponseException e) {
					messagesToRemove.add(messageId);
				}
			}

			for (String messageId : messagesToRemove) {
				System.out.println("[PollController][clearLostConfiguration] - Removing message " + messageId + " from channel " + channelId);
				config.channels.get(channelId).messages.remove(messageId);
			}
		}

		for (String channelId : channelsToRemove) {
			System.out.println("[PollController][clearLostConfiguration] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}

		saveConfiguration();
	}

	public void addToggleReaction(String textChannelId, String messageId, String emote) {
		if (!config.channels.containsKey(textChannelId)) {
			config.channels.put(textChannelId, new ChannelConfig());
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			channel.messages.put(messageId, new MessageConfig());
		}

		MessageConfig message = channel.messages.get(messageId);
		message.emotes.add(emote);

		saveConfiguration();
	}

	public void stopPoll(String textChannelId, String messageId) {
		if (!config.channels.containsKey(textChannelId)) {
			return;
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		channel.messages.remove(messageId);

		if (channel.messages.isEmpty()) {
			config.channels.remove(textChannelId);
		}

		saveConfiguration();
	}

	public String[] getReactsForMessage(String textChannelId, String messageId) {
		if (!config.channels.containsKey(textChannelId)) {
			return null;
		}

		ChannelConfig channel = config.channels.get(textChannelId);
		if (!channel.messages.containsKey(messageId)) {
			return null;
		}

		MessageConfig message = channel.messages.get(messageId);
		return message.emotes.toArray(new String[0]);
	}

	public void onGuildLeave(GuildLeaveEvent event) {
		List<String> channelsToRemove = new ArrayList<>();
		for (String channelId : config.channels.keySet()) {
			TextChannel textChannel = jda.getTextChannelById(channelId);
			if (textChannel != null) {
				if (textChannel.getGuild().getId().equals(event.getGuild().getId())) {
					channelsToRemove.add(channelId);
				}
			}
		}

		for (String channelId : channelsToRemove) {
			System.out.println("[PollController][onGuildLeave] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}

		saveConfiguration();
	}

	public void onChannelDelete(TextChannelDeleteEvent event) {
		String channelId = event.getChannel().getId();
		if (config.channels.containsKey(channelId)) {
			System.out.println("[PollController][onChannelDelete] - Removing channel " + channelId);
			config.channels.remove(channelId);
		}

		saveConfiguration();
	}

	public void onMessageDelete(MessageDeleteEvent event) {
		String channelId = event.getChannel().getId();
		if (!config.channels.containsKey(channelId)) {
			return;
		}

		String messageId = event.getMessageId();
		if (config.channels.get(channelId).messages.containsKey(messageId)) {
			System.out.println("[PollController][onMessageDelete] - Removing message " + messageId + " from channel " + channelId);
			config.channels.get(channelId).messages.remove(messageId);
		}

		saveConfiguration();
	}

	public void onReaction(MessageReactionAddEvent event) {
		String channelId = event.getTextChannel().getId();
		if (!config.channels.containsKey(channelId)) {
			return;
		}

		String messageId = event.getMessageId();
		ChannelConfig channel = config.channels.get(channelId);
		if (!channel.messages.containsKey(messageId)) {
			return;
		}

		String emoteName = event.getReactionEmote().isEmoji()
				? event.getReactionEmote().getEmoji()
				: event.getReactionEmote().getName() + ":" + event.getReactionEmote().getIdLong();
		if (!channel.messages.get(messageId).emotes.contains(emoteName)) {
			return;
		}

		MessageConfig message = channel.messages.get(messageId);

		if (message.lastUserVotes.containsKey(event.getUserId()) && !message.lastUserVotes.get(event.getUserId()).equals(emoteName)) {
			event.getTextChannel().removeReactionById(messageId, message.lastUserVotes.get(event.getUserId()), Objects.requireNonNull(event.getUser())).submit();
		}
		message.lastUserVotes.put(event.getUserId(), emoteName);

		saveConfiguration();
	}

	private static class Config {
		private final Map<String, ChannelConfig> channels = new HashMap<>();
	}

	private static class ChannelConfig {
		private final Map<String, MessageConfig> messages = new HashMap<>();
	}

	private static class MessageConfig {
		private final List<String> emotes = new ArrayList<>();

		private final Map<String, String> lastUserVotes = new HashMap<>();
	}
}
